var express = require('express');
var app     = express();
var maxAge  = 31557600000;
var path = require("path");

//app.use(express.compress());
//app.use(express.bodyParser());
//app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, '../')));
app.use(express.static(path.join(__dirname, '../','app')));
app.use('/bower_components', express.static(path.join(__dirname, '../', 'bower_components')));


app.get('/*', function(req,res)
{
    res.sendFile(path.join(__dirname, '../', 'app/dist/es/index.html'));
});

app.listen(3003);

console.log('Listening on port 3003');
