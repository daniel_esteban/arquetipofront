var jsonModules = require("./es/i18n-es"),
    _ = require('lodash'),
    esJson = {};

jsonModules.forEach(function(element, index, array){
   esJson = _.assignIn(esJson, element);
});

module.exports = {
    "es":esJson
}