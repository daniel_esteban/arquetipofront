var paths = {
    root: 'src/',      // App root path
    src: 'src/js/',   // Source path
    dist: 'dist/', // Distribution path
    test: 'test/',     // Test path
    annotate: 'annotate/',
    sourceCss: 'scss/',
    distCss: './dist/css'
};

var sassConfDev = {
        errLogToConsole: true,
        includePaths: ['node_modules/foundation-sites/scss', 'node_modules/slick-carousel/slick'],
        outputStyle: 'nested'
    },
    sassConfPro = {
        errLogToConsole: true,
        includePaths: ['node_modules/foundation-sites/scss', 'node_modules/slick-carousel/slick'],
        outputStyle: 'compressed'
    };
module.exports = {

    'paths': paths,
    'sassConf': {
        'dev': sassConfDev,
        'pro': sassConfPro,
        "srcMapDest": "."
    }

}