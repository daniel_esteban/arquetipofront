var gulp = require('gulp'),
    defineModule = require('gulp-define-module'),
    concat_json = require("gulp-concat-json"),
    browserSync = require('browser-sync');

gulp.task('i18gen', function(){

    return gulp.src('app/scripts/modules/**/**.i18n.json')
        .pipe(concat_json("i18n-es.json"))
        .pipe(defineModule('node', {
            name: function(filePath) { return "i18n-es"; }
        }))
        .pipe(gulp.dest('./i18n/es/'))
        .pipe(browserSync.stream());
});