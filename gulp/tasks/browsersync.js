var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    config = require('../config');

gulp.task('server',['compile'], function() {

    browserSync({
        server: {
            baseDir: ["./"]
            //directory: true
        },
        startPath: "/dist/es/",
        files: ['./dist/css/*.css']
    });

    gulp.watch(config.paths.sourceCss + '**/*.scss', ['sass']);
    gulp.watch(['app/scripts/modules/**/views/*.jade', 'app/*.jade','app/scripts/modules/**/**.i18n.json'], ['jade']);
    gulp.watch('app/scripts/modules/**/**.routes.json', ['routegen']);
    gulp.watch(['app/scripts/**/*js', 'app/scripts/*js'], ['js']);

});