var gulp = require('gulp'),
//config = require('../config'),
browserSync = require('browser-sync');

gulp.task('js', function () {

    return gulp.src(['app/scripts/**/*js', 'app/scripts/*js'])
        .pipe(gulp.dest('./dist/scripts'))
        .pipe(browserSync.stream());

});