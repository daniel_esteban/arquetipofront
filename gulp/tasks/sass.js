// -----------------------------------------------------------------------------
// Sass Compilation
// -----------------------------------------------------------------------------
'use strict';

var gulp 			= require('gulp'),
    sass 			= require('gulp-sass'),
    sourcemaps 		= require('gulp-sourcemaps'),
    autoprefixer 	= require('gulp-autoprefixer');

var	logger 			= require('../util/logger'),
    handleErrors	= require('../util/handleErrors');

var	config       	= require('../config'),
    browserSync = require('browser-sync');

/// -------------------------------------------------------------------------------------------------------


gulp.task('sass', function() {
    return gulp
    // Busca todos los ficheros '.scss' en el directorio de origen especificado en 'cssInput'
        .src(config.paths.sourceCss + '**/*.scss')
        // Inicializamos los mapas de código para el CSS a generar
        .pipe(sourcemaps.init())
        // Ejecuta sass en los ficheros recuperados
        .pipe(sass(config.sassConf.dev))
        // Control de errores
        .on('error', handleErrors)
        // Insertamos los prefijos por vendor en el CSS generado
        //.pipe(autoprefixer())
        // Escribe los mapas de código para el CSS generado
        .pipe(sourcemaps.write(config.sassConf.srcMapDest))
        // Escribe el CSS resultante en la ruta de salida especificada en 'cssOutput'
        .pipe(gulp.dest(config.paths.distCss))
        // Log file size
        .pipe(logger.fileSize('Generated style file'));

});