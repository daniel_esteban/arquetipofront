var gulp = require('gulp'),
    jade = require('gulp-jade'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync');


//console.log(i18n);
//config = require('../config'),
//browserSync = require('browser-sync');

gulp.task('jade',['i18gen'], function () {
    var  i18n = require('../../i18n/i18n');


    function compileViewTemplates(key) {
        var LOCALS = {i18n: i18n[key]};
        return gulp.src(['app/scripts/modules/**/views/*.jade'])
            .pipe(jade({
                locals: LOCALS,
                pretty: true
            })).pipe(rename(function (path) {
                path.basename += "-" + key;
            }))
            .pipe(gulp.dest('./dist/html'))
            .pipe(browserSync.stream());
    }

    function compileIndexTemplates(key) {
        var LOCALS = {i18n: i18n[key]};
        return gulp.src(['app/*.jade'])
            .pipe(jade({
                locals: LOCALS,
                pretty: true
            })).pipe(rename(function (path) {
                path.dirname += "/" + key;
            }))
            .pipe(gulp.dest('./dist/'))
            .pipe(browserSync.stream());
    }


    for(var key in i18n){
        // check also if property is not inherited from prototype
        if (i18n.hasOwnProperty(key)) {
            compileViewTemplates(key);
            compileIndexTemplates(key);
        }
    }
});