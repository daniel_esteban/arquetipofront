var gulp = require('gulp'),
    defineModule = require('gulp-define-module'),
    concat_json = require("gulp-concat-json"),
    browserSync = require('browser-sync');

gulp.task('routegen', function(){

    return gulp.src('app/scripts/modules/**/**.routes.json')
        .pipe(concat_json("allroutes.json"))
        .pipe(defineModule('amd', {
            name: function(filePath) { return "allroutes"; }
        }))
        .pipe(gulp.dest('./dist/scripts/'))
        .pipe(browserSync.stream());
});