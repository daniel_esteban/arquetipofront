angularjs-lazy-loading-with-requirejs
=====================================
A demonstration of lazy loading in [AngularJS](http://angularjs.org/) using [Asynchronous Module Definitions (AMD)](http://wiki.commonjs.org/wiki/Modules/AsynchronousDefinition) with [RequireJS](http://requirejs.org/).
This sample application was created to accompany the blog post entitled '[Lazy Loading In AngularJS](http://ify.io/lazy-loading-in-angularjs/)'. Note, however, that this is only meant to be a very basic (i.e., not production quality) example. 

## How it works
Simple specify your routes along with its lazy dependencies in `modules/**/module.route.js` and add the requirejs dependecy in app/routes.js

## How to run the demo
* Install node.js
* Run `npm install` from within the root project directory to install relevant dependencies
* Run `bower install` from within the root project directory to install relevant bower components
* Run `gulp server` from within the root project directory to start the app
* Run `gulp jade and gulp sass` to compile jade templates and scss files to app/dist
* Open `http://localhost:3003/` in the browser
