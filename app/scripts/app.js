define(['allroutes','services/dependencyResolverFor'], function(routesFiles, dependencyResolverFor)
{
    var app = angular.module('app', ['ngRoute']);

    app.config(
    [
        '$routeProvider',
        '$locationProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$provide',

        function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide)
        {
	        app.controller = $controllerProvider.register;
	        app.directive  = $compileProvider.directive;
	        app.filter     = $filterProvider.register;
	        app.factory    = $provide.factory;
	        app.service    = $provide.service;

            $locationProvider.html5Mode(false);
            if(routesFiles !== undefined)
            {
                routesFiles.forEach(function(file,index,array){
                    file.routes.forEach(function(route,index,array){
                        //TODO hacer dinamico el parametro de idioma de la template
                       $routeProvider.when(route.path, {templateUrl:route.templateUrl + "-es.html", resolve:dependencyResolverFor(route.dependencies)});
                   });
                });

            }
            $routeProvider.otherwise({redirectTo:'/'});
        }
    ]);

   return app;
});